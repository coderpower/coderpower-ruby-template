require "spec_helper"
require "./sources/addition.rb"

describe "Add two numbers" do
	it 'Must return "isNaN" if one of the arguments are not a number' do
		a = 'p'
		b = 32
		result = addition(a, b)
		expect(result).to eq('NaN')
	end
	it 'Must return a number' do
		a = 2
		b = 5
		result = addition(a, b)
		expect(result.is_a? Integer).to eq(true)
	end
	it 'Must return the right result' do
		a = 23
		b = 54
		result = addition(a, b)
		expect(result).to eq(77)
	end
end
