def addition(a, b)
	if (!a.is_a? Integer) || (!b.is_a? Integer)
		return "NaN"
	end

	return a + b
end
